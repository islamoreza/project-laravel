<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view ('halaman.register');
    }

    public function masuk(Request $request)
    {
        //dd($request->all());
        $namasatu = $request['nama1'];
        $namadua = $request ['nama2'];

        return view ('halaman.welcome', compact('namasatu','namadua'));
    }
}
