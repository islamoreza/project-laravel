<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view ('halaman.home');
    }

    public function table()
    {
        return view ('halaman.data-tables');
    }
}
