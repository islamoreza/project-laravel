@extends ('layout.master')
    @section ('title')
    REGISTER
    @endsection

    @section ('content')
    <h2>Buat Account Baru</h2>
    <form action="/welcome" method="post">
        @csrf
      <h4>Sign Up Form</h4>
        <label>First name :</label><br><br>
        <input type="text" name="nama1"><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="nama2"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br><br>
        <label>Nasionality</label><br><br>
        <select name="negara">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa">English <br>
        <input type="checkbox" name="bahasa">Other <br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
    @endsection